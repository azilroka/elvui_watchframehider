local E, L, V, P, G = unpack(ElvUI)
local WFH = E:NewModule("WatchFrameHider", "AceEvent-3.0")
local EP = LibStub("LibElvUIPlugin-1.0")
local addon = ...

P["watchframehider"] = {
	["enabled"] = true,
	["hidePvP"] = false,
	["hideArena"] = false,
	["hideParty"] = false,
	["hideRaid"] = false,
	["collapsePvP"] = true,
	["collapseArena"] = true,
	["collapseParty"] = true,
	["collapseRaid"] = true,
}

-- Hide Quest Tracker based on zone
function WFH:UpdateHideState()
	local WF = _G["WatchFrame"]
	
	if UnitAffectingCombat("player") then self:RegisterEvent("PLAYER_REGEN_ENABLED", "HandleEvent"); return end
	local Inst, InstType = IsInInstance();
	local Hide = false;
	if Inst then
		if (InstType == "pvp" and E.db.watchframehider.hidePvP) then			-- Battlegrounds
			Hide = true;
		elseif (InstType == "arena" and E.db.watchframehider.hideArena) then	-- Arena
			Hide = true;
		elseif (InstType == "party" and E.db.watchframehider.hideParty) then	-- 5 Man Dungeons
			Hide = true;
		elseif (InstType == "raid" and E.db.watchframehider.hideRaid) then	-- Raid Dungeons
			Hide = true;
		end
	end
	if Hide then WF:Hide() else WF:Show() end
end

-- Collapse Quest Tracker based on zone
function WFH:UpdateCollapseState()
	local WF = _G["WatchFrame"]
	
	if UnitAffectingCombat("player") then self:RegisterEvent("PLAYER_REGEN_ENABLED", "HandleEvent"); return end
	local Inst, InstType = IsInInstance();
	local Collapsed = false;
	if Inst then
		if (InstType == "pvp" and E.db.watchframehider.collasePvP) then			-- Battlegrounds
			Collapsed = true;
		elseif (InstType == "arena" and E.db.watchframehider.collapseArena) then	-- Arena
			Collapsed = true;
		elseif (InstType == "party" and E.db.watchframehider.collapseParty) then	-- 5 Man Dungeons
			Collapsed = true;
		elseif (InstType == "raid" and E.db.watchframehider.collapseRaid) then	-- Raid Dungeons
			Collapsed = true;
		end
	end
	
	if Collapsed then
		WF.userCollapsed = true;
		WatchFrame_Collapse(WF);
	else
		WF.userCollapsed = false;
		WatchFrame_Expand(WF);
	end	
end

function WFH:HandleEvent(event)
	self:UpdateHideState()
	self:UpdateCollapseState()
	if event == "PLAYER_REGEN_ENABLED" then self:UnregisterEvent("PLAYER_REGEN_ENABLED") end
end

function WFH:GetOptions()
	E.Options.args.watchframehider = {
        order = 85,
        type = "group",
        name = L["WatchFrameHider"],
        args = {
            header = {
                order = 1,
                type = "header",
                name = L["ElvUI WatchFrameHider by Sortokk"],
            },
            description = {
                order = 2,
                type = "description",
                name = L["ElvUI WatchFrameHider hides or collapses the watch frame based on your configuration.\n"],
            },
            general = {
                order = 3,
                type = "group",
                name = L["General"],
                guiInline = true,

                args = {
                    enabled = {
                        type = "toggle",
                        order = 1,
                        name = L["Enable"],
                        desc = L["Enable the watch frame hider."],
                        get = function(info) return E.db.watchframehider[info [#info] ] end,
                        set = function(info,value) E.db.watchframehider[info [#info] ] = value; WFH:Enable(); end,
                    },
                },
            },
            watchFrameHiderOptions = {
            	order = 4,
            	type = "group",
            	name = L["Watch Frame Hider Options"],
            	guiInline = true,
            	get = function(info) return E.db.watchframehider[info [#info] ] end,
            	set = function(info, value) E.db.watchframehider[info [#info] ] = value; end,
            	args = {
            		hidePvP = {
				    	type = "toggle",
				    	order = 5,
				    	name = L["Hide during PvP"],
				    	desc = L["Hide the watch frame during PvP (i.e. Battlegrounds)"],
				    },
            		hideArena = {
				    	type = "toggle",
				    	order = 6,
				    	name = L["Hide in arena"],
				    	desc = L["Hide the watch frame when in the arena"],
				    },
            		hideParty = {
				    	type = "toggle",
				    	order = 7,
				    	name = L["Hide in dungeon"],
				    	desc = L["Hide the watch frame when in a dungeon"],
				    },
            		hideRaid = {
				    	type = "toggle",
				    	order = 8,
				    	name = L["Hide in raid"],
				    	desc = L["Hide the watch frame when in a dungeon"],
				    },
            		collapsePvP = {
				    	type = "toggle",
				    	order = 1,
				    	name = L["Collapse during PvP"],
				    	desc = L["Collapse the watch frame during PvP (i.e. Battlegrounds)"],
				    },
            		collapseArena = {
				    	type = "toggle",
				    	order = 2,
				    	name = L["Collapse in arena"],
				    	desc = L["Collapse the watch frame when in the arena"],
				    },
            		collapseParty = {
				    	type = "toggle",
				    	order = 3,
				    	name = L["Collapse in dungeon"],
				    	desc = L["Collapse the watch frame when in a dungeon"],
				    },
            		collapseRaid = {
				    	type = "toggle",
				    	order = 4,
				    	name = L["Collapse in raid"],
				    	desc = L["Collapse the watch frame during a raid"],
				    },
            	}
            },
        },
    }
end

function WFH:Enable()
	if(UnitAffectingCombat("player")) then self:RegisterEvent("PLAYER_REGEN_ENABLED", "Enable"); return end
	if E.db.watchframehider.enabled then
		self:RegisterEvent("PLAYER_ENTERING_WORLD", "HandleEvent")
	else
		self:UnregisterEvent("PLAYER_ENTERING_WORLD")
	end
	self:UnregisterEvent("PLAYER_REGEN_ENABLED")
end

function WFH:Initialize()
	EP:RegisterPlugin(addon,WFH.GetOptions)
	WFH:Enable()
end

E:RegisterModule(WFH:GetName())